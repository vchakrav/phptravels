package com.oops.abstraction;

public class Honda extends Bike {
	
	@Override
	public void handle() {
		
		System.out.println("All Bikes have handle");
				
	}

	@Override
	public void run() {
		
		System.out.println("All Bikes run on engines");
	}

	public static void main(String[] args) {
		
		Honda b1 = new Honda();
		b1.handle();
		b1.run();
		b1.engine();

	}

	

}
