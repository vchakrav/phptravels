package com.oops.abstraction;

public abstract class Bike {
	
	public void engine(){
		
		System.out.println("All Bikes Have Engines");
					
	}
	
	public abstract void handle();
	public abstract void run();

}
